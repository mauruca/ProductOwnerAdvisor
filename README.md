# Product Owner Advisor
The advisor purpose is estimate the value and the amount of effort to deliver a backlog item.

## Product Vision
To product owners or decision takers that need prioritize backlog's stories, the PO Advisor is a analysis system that uses machine learning algorithms to do it. It understands the value and cost of the proposal, and then find the ROI.

## Product Backlog
- Estimate story execution cost
- Estimate story business value
- Prioritize the backlog according to highest priority value  

> priority = story business value / story execution cost

## Machine Learning Algorithms Online APIs
[Google NL](https://cloud.google.com/natural-language/) - Needs credit card to sign and test for 60 days.  
[Bluemix Alchemi](https://console.ng.bluemix.net/catalog/services/alchemy_api) [API Language](http://www.alchemyapi.com/products/alchemylanguage) - Free for small amounts of tests.  
[textrazor](https://www.textrazor.com/plans) - Free for 500 request per day.  
[nltk](https://github.com/nltk/nltk)  
[Watson integration](https://www.ibm.com/watson/developercloud/services-catalog.html) - Charge for tests.  

## Links
[Kaggle](https://www.kaggle.com/competitions)

## Machine Learning Courses
[Coursera ML Stanford](https://www.coursera.org/learn/machine-learning)

## [Training Data](https://github.com/maurucao/ProductOwnerAdvisor/blob/master/backlogExamples.md)
Example: `As <person>, I can <create, see, list, ...> <something> to <reason if necessary>.`

## Algorithms

[Linear Regression][1]

###NLP

####AlchemyLanguage
Entity Extraction  
Sentiment Analysis  
Emotion Analysis  <========
Keyword Extraction  
Concept Tagging		<====  
Relation Extraction  
Taxonomy Classification	<====  
Author Extraction  
Language Detection	<====  
Text Extraction  
Microformats Parsing  
Feed Detection  
Linked Data Support  

####Natural Language Classifier
The service interprets the intent behind text and returns a corresponding classification with associated confidence levels.  

####Personality Insights
The service outputs personality characteristics that are divided into three dimensions: the Big 5, Values, and Needs.  

####Tradeoff Analytics
The service uses a mathematical filtering technique called “Pareto Optimization,” that enables users to explore tradeoffs when considering multiple criteria for a single decision.  

## Papers

[Investigating Machine Learning Approaches for Sentence Compression in Different Application Contexts for Portuguese][3]

## Development Environment
Replace ~/workspace/ProductOwnerAdvisor/trainingData path according to your workspace.  
`docker run -it -v ~/workspace/ProductOwnerAdvisor/trainingData:/var/POAdvisor/trainingData -v ~/workspace/ProductOwnerAdvisor/NLTK:/var/POAdvisor/NLTK python:alpine /bin/sh`

## 23/01/2017
Coca Pitzer  
Mauricio Pinheiro  
Mayron Muylaert  

### Pontos em aberto
- Coca conversou com Aurélio que trabalha com o Watson e ele gostou e topou a ideia, mas ele estava entrando de férias. Vamos marcar um evento um HH para falar do projeto.
- Não conseguimos aplicar energia ainda na produço do mvp. A semana seguinte ainda será complicada para os participantes. Fica ainda o primeiro passo a ser dado.

### Pontos da última reunião
- Avaliar algoritmos utilizados em bots. ==> Encontramos esse site [BotStash](http://www.botsfloor.com/botstash/) que tem a lista de [ferramentas de NLP](https://tutorials.botsfloor.com/the-best-nlp-language-understanding-tools-to-make-your-chatbot-smarter-d6db34fbe90d#.6ghjp2v8n) para usar em bots.

- Estudo de modelo de como analisar os dados. ==> Ao avaliar os dados, podemos criar um modelo baseado em quem, ação, objeto, e para que ou porquê. Tentando se aproximar do modelo golden circle(why, how, what) e adicionando o who. Pelo modelo utilizado até o momento (Example: `As <person>, I can <create, see, list, ...> <something> to <reason if necessary>.`) ficaríamos com who, what e why, sendo o how é exatamente a implementação.

Se forem adicionados os dados de valor e custo às sentenças podemos usar uma [regressão linear][1] para o problema. Para isso teríamos que utilizar um algoritmo que avaliaria a semantica de algumas palavras para definir um índice numérico.

![Fluxo de avaliação de algoritmo][2]

### Pontos abordados:
-

### Para o próximo
-

## 13/01/2017
Coca Pitzer  
Mauricio Pinheiro  
Mayron Muylaert  

### Pontos em aberto
- Coca conversou com Aurélio que trabalha com o Watson e ele gostou e topou a ideia, mas ele estava entrando de férias. Vamos marcar um evento um HH para falar do projeto.
- Não conseguimos aplicar energia ainda na produço do mvp. A semana seguinte ainda será complicada para os participantes. Fica ainda o primeiro passo a ser dado.

### Pontos da última reunião
- Aplicar NLP em um exemplo e ver o resultado. ==> Não realizado

### Pontos abordados:
- Dificuldade de entender como utilizar NLP no trabalho. Não é facil aplicar diretamente a aplicação de teste online do Bluemix Alchemy e do Textrazor não retorna algo que parece ser util.

### Para o próximo
- Avaliar algoritmos utilizados em bots.
- Estudo de modelo de como analisar os dados.

## 19/12/2016
Coca Pitzer  
Mauricio Pinheiro  
Mayron Muylaert  

### Pontos abordados:
- Coca conversou com Aurélio que trabalha com o Watson e ele gostou e topou a ideia, mas ele estava entrando de férias. Vamos marcar um evento um HH para falar do projeto.  
- Não conseguimos aplicar energia ainda na produço do mvp. A semana seguinte ainda será complicada para os participantes. Fica ainda o primeiro passo a ser dado.

### Para o próximo
- Aplicar NLP em um exemplo e ver o resultado.

## 12/12/2016
Coca  
Mauricio  

### Pontos abordados:
- Coca vai conversar com o pessoal do Watson para saber dicas.
- Vamos buscar alternativas ao Watson para o NLC.

https://cloud.google.com/natural-language/

http://www.alchemyapi.com/products/alchemylanguage
https://console.ng.bluemix.net/catalog/services/alchemy_api

https://www.textrazor.com/plans

https://github.com/nltk/nltk

### Para o próximo
- Aplicar NLP em um exemplo e ver o resultado

## 05/12/2016
Coca  
Mauricio  

### Pontos abordados:
- Uso do Watson a princípio está limitado a um teste de 30 dias.

### Para o próximo
- Coca vai conversar com o pessoal do Watson para saber dicas.
- Vamos buscar alternativas ao Watson para o NLC.

## 25/11/2016
Coca  
Mauricio  

### Pontos abordados:
- Decidimos que o MVP será para avliar o custo da história e vamos utilizar Natural Language Classifier.
- Conseguimos compartilhar o RealtimeBoard \o/
- Construir o training data de histórias com custo.

### Para o próximo
- Coca vai conversar com o pessoal do Watson para que o pessoal nos mostre como fazer e complexidade
- Mauricio vai avaliar o knowledge studio para comparar com nlc.

## 21/11/2016
Coca  
Mauricio

### Pontos abordados:
- Realtimeboard https://realtimeboard.com já tem o board compartilhado, porem a ferramenta cobra para uso em time.
- Valores da agilidade(offtopic).
- Mudanças nas empresas.(offtopic) 4th Industrial Revolution(https://www.weforum.org/agenda/2016/01/the-fourth-industrial-revolution-what-it-means-and-how-to-respond/)
- Proposta do Mauricio seria obter dados de backlog, escolher um primeiro algoritmo para análise para trabalhar.

### Para o próximo
- Avaliar ambiente de desenvolvimento.
- Escolher algoritmo inicial.

## 7/11/2016 - Inicio
Coca  
Mauricio

### Pontos abordados:
- Devemos avaliar valor de cada história e o esforço para priorizar o backlog
- Como entender o que está no texto? como dar valor ao texto?
- quais algoritmos seriam teriam o impacto desejado.
- decidimos partir para uma execução por tentativa e erro em um modelo pequeno.
- construir um esboço de um roadmap priorizando os pontos que o advisor deve atender
- Entendemos que pode existir uma dependencia entre o primeiro e o segundo.

Roadmap
- construir o backlog com perguntas
- mensurar o valor das histórias
- mensurar o esforço das histórias
- priorizar o backlog -> Coca, fiquei tentando lembrar o ultimo ponto. É isso? O hangout nao me deixou ver o que escrevermos lá dentro da reunião :(. Melhor usar como vc fez colocando o texto fora da tela da reunião.

### Para o próximo
- Trazer um backlog de exemplo
- Avaliar quais algoritmos para construção de backlog, valor e custo.
- Para priorizar precisamos minimamente de valor e custo?

### Algoritmos para custo
- No slack de agilidade, no canal de Kanban, estavam conversando do dia 10/11/16 sobre #noestimates e o uso de algoritmos probabilisticos. Falam do uso de simulação de montecarlo para previsão do custo.
https://lizkeogh.com/2015/05/01/the-estimates-in-noestimates/
https://twitter.com/lki_dja/status/778391867524194304

Sempre precisamos de dados de estimativas já realizadas para poder gerar novas? Neste caso precisaríamos definir features?  
É possivel avaliar custo a partir do texto? Sem uso de features específicas? Ou mesmo colher as features do texto?  

[1]: https://en.wikipedia.org/wiki/Linear_regression "Linear Regression"
[2]: /images/fluxo.png "Fluxo de avaliação de algoritmo"
[3]: /papers/PROPOR2016-NobregaPardo.pdf
