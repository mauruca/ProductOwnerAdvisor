# Product Owner Advisor
The advisor purpose is estimate the value and the amount of effort to deliver a backlog item.

## Product Backlog Examples links
https://www.mountaingoatsoftware.com/agile/scrum/product-backlog/example  
http://www.scrum-institute.org/The_Scrum_Product_Backlog.php  

## Examples files
[mountaingoat](https://github.com/maurucao/ProductOwnerAdvisor/blob/master/trainingData/trainingBacklog)
