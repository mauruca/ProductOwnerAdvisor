# Product Owner Advisor
The advisor purpose is estimate the value and the amount of effort to deliver a backlog item.

## API Evaluation

Trying to figure out how to identify the importance of the text or words to calculate a cost to build the story. There are some options:  

### Supervised learning
- Define the cost of each story given the amount of similar words.

### Unsupervised
- 

###[Bluemix Alchemi](https://console.ng.bluemix.net/catalog/services/alchemy_api) [API Language](http://www.alchemyapi.com/products/alchemylanguage) - Free for small amounts of tests.

Tests were made online using the sentence below.  
`As a site administrator, I can read practicing and training applications and approve or reject them.`

The document entity administrator was identified as a job title. The algorithm related applications just for training and not for practicing. It also identified them as a person not related to applications. The correct identification of entities and objectives could help us to classify the sentence.  

###[textrazor](https://www.textrazor.com/plans)  

Tests were made online using the sentence below.  
`As a site administrator, I can read practicing and training applications and approve or reject them.`

The algorithm identified word "site" as a "web site" with low confidence. It gives a lot of similar words options and the confidence about them. The dependency sounds good to understand the relation of words but it does not identify practice as an application.
